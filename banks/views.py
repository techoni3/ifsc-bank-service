from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from .models import Banks, Branches
from django.views import View


class Banks(View):
    def get(self, request):
        payload = {"data": []}
        try:
            ifsc = request.GET.get('ifsc')
            if ifsc is not None:
                branches = Branches.objects.filter(ifsc=ifsc.upper())
            else:
                city = request.GET['city'].upper()
                name = request.GET['bank'].upper()
                branches = Branches.objects.filter(city=city, bank__name=name)
            for b in branches:
                d = {
                    'bank': b.bank.name,
                    'ifsc': b.ifsc,
                    'branch': b.branch,
                    'address': b.address,
                    'city': b.city,
                    'district': b.district,
                    'state': b.state
                }
                payload["data"].append(d)
            payload["msg"] = "Branch details fetched successfully."
            return JsonResponse(payload, status=200)
        except (KeyError, AttributeError) as k:
            payload["msg"] = "Please specify ifsc or (city, bank)."
            return JsonResponse(payload, status=400)
        except Exception as e:
            payload["msg"] = "Something Went Wrong. Try Again Later."
            return JsonResponse(payload, status=500)            